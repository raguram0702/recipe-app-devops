variable "prefix" {
  default = "rad"
}

variable "project" {
  default = "recipe_app_devops"
}

variable "contact" {
  default = "raguram0702@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "932803920944.dkr.ecr.us-east-1.amazonaws.com/recipe-app-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "932803920944.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}
