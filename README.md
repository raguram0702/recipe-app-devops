# Django Web Framework Application

This application is built using the Python-based Django web framework and utilizes Docker for containerization and AWS for cloud deployment. Terraform is used for infrastructure as code and deployment management.

## Prerequisites
- Python 3.6 or higher
- Docker
- AWS account and credentials
- Terraform

### Installation

Clone this repository to your local machine:

```
git clone https://gitlab.com/raguram0702/recipe-app-devops.git
```

### Deployment

To deploy this application to AWS, you will need to have Terraform installed and have your AWS credentials configured.

Navigate to the deploy directory and create a terraform.tfvars file. Add the necessary variables from sample.vars

## Terraform (via Docker Compose)

```
docker-compose -f deploy/docker-compose.yml terraform init
```

Apply the Terraform configuration:

```
docker-compose -f deploy/docker-compose.yml run --rm terraform apply
```

## Bastion Commands

### Authenticate with ECR

In order to pull the application image, authentication with ECR is required.

To authenticate with ECR:

```sh
$(aws ecr get-login --no-include-email --region us-east-1)
```

### Create a superuser 

Replace the following variables:
 * `<DB_HOST>`: The hostname for the database (retrieve from Terraform apply output)
 * `<DB_PASS>`: The password for the database instance (retrieve from GitLab CI/CD variables)

```bash
docker run -it \
    -e DB_HOST=<DB_HOST> \
    -e DB_NAME=recipe \
    -e DB_USER=recipeapp \
    -e DB_PASS=<DB_PASS> \
    <ECR_REPO>:latest \
    sh -c "python manage.py wait_for_db && python manage.py createsuperuser"
```

The application should now be deployed and accessible at the api_endpoint URL provided by Terraform Staging apply job in Gitlab CI.

### Destroy

Remove any resources managed by Terraform (tear down all infrastructure) for selected workspace

```sh
docker-compose -f deploy/docker-compose.yml terraform destroy
```

### Built With
- [Django](https://www.djangoproject.com/) - The web framework used
- [Docker](https://www.docker.com/) - Containerization
- [AWS](https://aws.amazon.com/) - Cloud deployment
- [Terraform](https://www.terraform.io/) - Infrastructure as code and deployment management


### License
This project is licensed under the MIT License - see the [LICENSE](https://gitlab.com/raguram0702/recipe-app-devops/-/blob/fd440f5113081d64d7bc9082d28d8c7fed8eb04d/LICENSE) file for details.
